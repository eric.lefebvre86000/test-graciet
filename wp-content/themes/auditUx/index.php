<?php get_header(); ?>

<div class="blocolor" style="background-color:#007256;position:relative">
  <p style="color:#FFFFFF;position:absolute;top:25%;left:32%;font-size:45px;">Audit UX (expérience utilisateur)</p>
  <p style="color:#FFFFFF;position:absolute; top:40%;left:40%;font-size:45px;">de votre site web</p>
  <a href="#"><img style ="position:absolute; top:60%; left:40%" src="<?php echo get_template_directory_uri(); ?>/img/test.svg" alt="bouton test"></a>
</div>
<div class="bloctext" style="height:692px;position:relative">
  <img src="<?php echo get_template_directory_uri(); ?>/img/blur-close-up-focus-gadget-574285.jpg" alt="audit" width="588px" height="353px">
  <h5 style="position:absolute;color:#02544D;font-size:28;top:0;left:53%;font-weight: bold;">Qu'est ce qu'un audit UX?</h5>
  <p style="position:absolute;top:10%;left:53%">Notre audit UX (pour expérience Utilisateur) vise à évaluer une (ou des) interface(s)dans le but de diagnostiquer
     d'éventuels problèmes d'ergonomie, d'utilisabilité, de pertinence, de performance ou d'accessibilité présents sur un site web.</p>
  <p style="position:absolute;top:27%;left:53%">Comme pour un contrôle technique, nous analysons les pages de votre site à l'aide de critères et de recommandations issus de la
    recherche (critères ergonomiques de Bastien et Scapin, heuristiques de Jacod Nielsen) ou développés par nos experts en UX research.</p>
  <p style="position:absolute;top:45%;left:53%">En cas de problèmes identifiés, nous vous proposons des préconisations pour améliorer l'expérience utilisateur de votre site.</p>
  <div class="audit">
    <p style="position:absolute; top:25%;left:41%; color:#FFFFFF;font-size:28px;font-weight: bold;">Ça m'intéresse !</p>
    <a href="#" style="position:absolute; bottom:20%; left:39%"><img src="<?php echo get_template_directory_uri(); ?>/img/audit.svg" alt="Demander un audit"/></a>
  </div>
</div>
<div class="blocolor" style="background-color:#FBF8EC;position:relative">
  <div class="text">
    <h5 style="color:#02544D;font-size:28px; font-weight:bold; position:absolute; top:10%;">Pourquoi recourir à un audit UX ?</h5>
    <div class="littlebloc">
      <h6 style="color:#029D77;font-size:24px;font-weight: bold;margin-top:22%">Objectifs</h6>
        <p><i class="fas fa-check" style="color:#029D77"></i> Vous permettre de concevoir un produit respectueux des grands principes d'ergonomie des interfaces et adapté à ses utilisateurs;</p>
        <p><i class="fas fa-check" style="color:#029D77"></i> Mettre le doigt sur des problèmes d'utilisation ou d'ergonomie qui contrarient l'expérience utilisateur sur une interface ou un site déjà en ligne;</p>
        <p><i class="fas fa-check" style="color:#029D77"></i> Bénéficier de recommandations précises et concrètes pour améliorer l'expérience utilisateur de votre site;</p>
        <p><i class="fas fa-check" style="color:#029D77"></i> Sensibilier vos équipes aux enjeux de l'expérience utilisateur.</p>
    </div>
    <div class="littlebloc">
      <h6 style="color:#029D77;font-size:24px;font-weight: bold;margin-top:22%;">Bénéfices</h6>
      <p><i class="fas fa-check" style="color:#029D77"></i> Contribuer à rassurer les utilisateurs et augmenter la confiance qu'ils placent dans votre entreprise;</p>
      <p><i class="fas fa-check" style="color:#029D77"></i> Diminuer le nombre de sollicitaions d'assistance de la part des utilisateurs de votre site;</p>
      <p><i class="fas fa-check" style="color:#029D77"></i> Augmenter le taux de transformation de votre site;</p>
      <p><i class="fas fa-check" style="color:#029D77"></i> Augmenter la fidélisation des utilisateurs.</p>
    </div>
  </div>
</div>
<div class="bloctext" style="height:418px">
  <div class="littlebloc">
    <h5 style="color:#02544D;font-size:28px; font-weight:bold;margin-bottom:20px;">A quel moment réaliser un audit de l'expérience utilisateur ?</h5>
    <p>Si l'idéal est d'intevenir le plus tôt possible, il est possible de réaliser ce type d'audit ergonomique à tout moment et même à plusieurs moments de la vie de vitre site, par exemple:</p>
    <p><i class="fas fa-check" style="color:#029D77"></i> Dès la conception du design de votre site (sur maquette ou prototypes);</p>
    <p><i class="fas fa-check" style="color:#029D77"></i> Avant la refonte de votre site;</p>
    <p><i class="fas fa-check" style="color:#029D77"></i> Avant la mise en production de nouveaux modèles de pages ou d'une nouvelle fonctionnalité;</p>
    <p><i class="fas fa-check" style="color:#029D77"></i> En cas de doute sur la qualité de l'expérience utilisateur ou de l'ergonomie proposée sur votre site.</p>
  </div>
  <div class="littleblocimage" >
    <p style="color:#FFFFFF;font-size:28px;font-weight: bold;margin-top:100px;">Une question?</p>
    <p style="color:#FFFFFF;font-size:28px;font-weight: bold;">Besoin d'en savoir plus?</p>
    <a href="#" style=""><img src="<?php echo get_template_directory_uri(); ?>/img/contacteznous.svg" alt="Contactez nous"/></a>
  </div>
</div>
<div class="blocolor" style="background-color:#FBF8EC">
  <div class="text">
    <div class="littlebloc">
        <h5 style="color:#02544D;font-size:28px; font-weight:bold;margin-top:10%;">Sur quels critères d'évaluation UX votre site est-il analysé ?</h5>
        <p>Notre méthode d'analyse ergonomique repose sur l'utilisation d'une grille. Cette grille d'analyse élaborée par nos experts est construite sur la base des éléments suivants:</p>
        <p>- <a href="#">critères ergonomique de Bastien et Scapin</a> (INRIA);<br>
           - <a href="#">heuristiques de Jacob Nielsen</a> (Nielsen/Norman Group);<br>
           - Critères issus de la <a href="#">chek-list Qualité Web d'Opquast</a> ;<br>
           - Autre critères liés au design de l'expérience utilisateur inspirés de notre R&D et de notre expérience terrain</p>
        <p>Les critères d'évaluation qui constituent cette grille sont classés en plusieurs thématiques: architecture et structuration de l'information, liens, navigation, contenus, lisibilité, guidage, contrôle, homogénéité/adaptabilité, charge mentale, accessibilité, données & vie privée, repsonsive et performance.</p>
    </div>
    <div class="littlebloc">
      <h5 style="color:#029D77;font-size:28px; font-weight:bold;margin-top:10%;margin-bottom:25px;">À savoir</h5>
      <p><i class="fas fa-check" style="color:#029D77"></i> Cette grille d'analyse n'est pas figée.Les critères sont régulièrement mis à jour grâce à l'avancement de la recherche de de notre R&D. Ils peuvent également être adaptés en fonction du type d'interface observé, de besoins utilisateurs particuliers ou de votre problématique ;</p>
      <p><i class="fas fa-check" style="color:#029D77"></i> Certains critères de notre grille concernent à la fois l'expérience utilisateur et le SEO. Ce sont des critères dits "SXO"(SEO +UX). Nos experts sont donc en mesure de mettre en lumière des problèmes de stratégie ou de conception ayant un impact sur votre référencement naturel. Pour en savoir plus, sachez que notre agence vous propose un <a href="#">audit SEO</a></p>
    </div>
  </div>
 </div>
 <div class="bloctext" style="height:418px">
   <h5 style="color:#02544D;font-size:28px; font-weight:bold;">Comment se déroule un audit UX chez Graciet & Co ?</h5>
   <img class="tableau" src="<?php echo get_template_directory_uri(); ?>/img/Onglets.svg" alt="audit">
 </div>
 <div class="parallax">
   <div class="blocparallax">
     <div class="textparallax">
       <h5 style="color:#FFFFFF;font-size:28px; ; font-weight:bold;">Combien coûte un audit de l'expérience utilisateur</h5><br>
       <p style="color:#FFFFFF;font-size:16px;">Le prix d'un audit UX est variable, il est déterminé en fonction du volume de pages ou modèles de pages à analyser.<br>
       Pour en savoir plus, <strong>contactez notre service commercial</strong> pour nous précisez votre besoin et recevoir un devis personnalisé. Nous vous livrerons un premier retour sous 24h.</p><br>
       <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/devis.svg" alt="Demandez un devis"></a>
     </div>
   </div>
 </div>
<div class="bloctext" style="height:1145px">
  <center><h5 style="color:#02544D;font-size:28px; font-weight:bold;">Notre audit UX vous intéresse?</h5><center>
  <center><h5 style="color:#02544D;font-size:28px; font-weight:bold;">Contactez-nous!</h5><center>
  <center><p style="color:#4D4D4D; font-size:16px; margin-top:40px;">Vous vous posez des questions sur l'expérience utilisateur de votre (futur) site <br> web et souhaitez recourir à un audit UX? Faites nous part de votre besoin,<br> notre équipe commerciale vous recontactera dans les plus brefs délais!</p></center>
  <div class="formulaire">
    <div class="bande">
      <h5 style="color:#02544D;font-size:28px;position: absolute;margin-left: 25px;margin-top: 20px;font-weight:bold;">Demande d'information</h5>
    </div>
    <h5 style="color:#02544D;font-size:28px;margin-top: 30px;font-weight:bold;float:left; margin-left: 25px;;">Vos coordonnées</h5>
    <form action="#" method="post">
    </form>
  </div>
  <div class="coordonnée"></div>
</div>
<div class="blocolor" style="height:280px;margin-bottom:80px;width:100%">
  <div class="littlebloc" style="background-color:#007256;height:100%;width:50%">
    <h5 style="color:#FFFFFF;font-size:28px; margin-top: 70px;margin-left: 37%;font-weight:bold;"><i class="fas fa-envelope"></i> Abonnez-vous à notre newsletter</h5>
    <p style="color:#FFFFFF;font-size:16px; margin-top: 25px;margin-left: 37%;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p>
  </div>
  <div class="littlebloc" style="background-color:#F8F3E1;height:100%;width:50%">
  </div>
</div>
<?php get_footer(); ?>
