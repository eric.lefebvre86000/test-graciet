<?php wp_footer();?>
  <footer>
      <div class="cadre">
        <div class="footercontainer">
          <h4 style="position: absolute; top:25%;">Suivez-nous sur les réseaux sociaux
            <span style="font-size: 1.5em; color :#029D77">
              <i class="fab fa-facebook-square"></i>
            </span>
            <span style="font-size: 1.5em; color :#029D77 ">
              <i class="fab fa-twitter-square"></i>
            </span>
            <span style="font-size: 1.5em; color :#029D77 ">
              <i class="fab fa-linkedin"></i>
            </span>
          </h4>
        </div>
      </div>
      <div class="footercontainer">
        <div class="bloc">
          <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Logo">
          <p style="margin-top:15px">L'agence Graciet & Co accompagne votre transformation digitale tout en développant votre visibilité sur le web</p>
          <p style="margin-top:20px">Référencement naturel, content marketing, UX design, et data analyse, nous mettons notre savoir-faire au service de votre ctroissance numérique!</p>
          <p style="margin-top:80px">© Copyright 2019 Graciet & Co</p>
        </div>
        <div class="bloc" style="background-color :#FFFFFF; height:400px; margin-top:35px; position: relative;">
          <h5 style="color:#02544D;position: absolute;top:40px;left:15%;font-weight: bolder">Nos coordonnées</h5>
          <ul style= "position: absolute;top:30px;left:-4%">
            <li style="margin-top:60px"><span style="color :#029D77"><i class="fas fa-envelope"></i></span> contact@graciet.info</li>
            <li style="margin-top:10px"><span style="color :#029D77"><i class="fas fa-phone"></i></span> +33 9 72 66 57 94</li>
            <li style="margin-top:10px;margin-left:7px;"><span style="color :#029D77"><i class="fas fa-map-marker-alt"></i></span> Agence de Poitiers<br>15 avenue René Cassin<br>86 961 FUTUROSCOPE CEDEX</li>
            <li style="margin-top:10px;margin-left:35px"><span style="color :#029D77"><i class="fas fa-map-marker-alt"></i></span> Agence de Paris<br>1 impasse de Reille<br>75014 PARIS</li>
          </ul>
          <a href="#"><img style="position:absolute;left:15%;bottom:20px" src="<?php echo get_template_directory_uri(); ?>/img/Boutonsecondaire.svg" alt="contact"></a>
        </div>
        <div class="bloc" style="position: relative";>
          <h5 style="color:#02544D;position: absolute;top:14px;left:15%;font-weight: bolder">Plus d'informations</h5>
          <ul style="position: absolute;top:65px;left:-4%">
            <li style="margin-bottom: 20px;"><a style="color: #02544D" href="#">Mentions Légales</a></li>
            <li style="margin-bottom: 20px;"><a style="color: #02544D" href="#">Politique de confidentialité</a></li>
            <li style="margin-bottom: 20px;"><a style="color: #02544D" href="#">Condition générales de vente</a></li>
          </ul>
        </div>
    </div>
  </footer>
</body>
