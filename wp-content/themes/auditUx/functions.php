<?php
if ( ! function_exists( 'carbon_scripts' ) ) :
/**
 * Enqueues stylesheets
 **/
function carbon_scripts(){
     wp_enqueue_style( 'carbon_styles', get_stylesheet_uri(), array(), null );
}
endif;

add_action( 'wp_enqueue_scripts', 'carbon_scripts' );

function register_my_menu(){
  register_nav_menu( 'main-menu', 'Menu principal' );
}
add_action( 'after_setup_theme', 'register_my_menu' );

add_theme_support( 'post-thumbnails' );

add_theme_support( 'title-tag' );
