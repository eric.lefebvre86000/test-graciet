<?php
wp_head();
?>
<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Audit Ux</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <?php wp_head(); ?>
  </head>
  <body>
    <header>
      <div class="bandeau">
        <a href="#">
          <img class="logo" src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Logo">
        </a>
        <div class="menu">
            <ul>
              <li><a style="color:#02544D; font-weight: bolder; text-decoration: none" href="#">Accueil</a></li>
              <li><a style="color:#02544D; font-weight: bolder; text-decoration: none" href="#">Agence</a></li>
              <li class="deroulant"><a style="color:#02544D; font-weight: bolder; text-decoration: none" href="#">Expertises</a>
                  <!--<ul class="sous">
                    <li><a href="#">Capter</a></li>
                    <li><a href="#">Convertir</a></li>
                    <li><a href="#">Auditer</a></li>
                  </ul>-->
              </li>
              <li><a style="color:#02544D; font-weight: bolder; text-decoration: none" href="#">Formations</a></li>
              <li><a style="color:#02544D; font-weight: bolder; text-decoration: none" href="#">Actualités</a></li>
            </ul>
          </div>
        <div class="tel">
          <i class="fas fa-phone" style="color:#029D77"></i>
          <img src="<?php echo get_template_directory_uri(); ?>/img/tel.svg" alt="Contact"/>
        </div>
        <a href="#"><img class="contact" src="<?php echo get_template_directory_uri(); ?>/img/Boutoncontact.svg" alt="Contact"/></a>
        <div class="sousmenu"></div>
        </div>
    </header>
