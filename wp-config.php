<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'graciet_audit_ux' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '6i6Q>IP+sM bJ*([av)Uk4Dh%J-sk)w+jrTF`p~]-eEe$Yy<dWImRE}ihDUa}CND' );
define( 'SECURE_AUTH_KEY',  'Gf|{.w0K(Vw)_ PUhW,(iXpWGah.n3/(QCD5TZ2#?Y<$r2*8t/E7t0qd9^1TfMV&' );
define( 'LOGGED_IN_KEY',    'XO#j/@ZAE&AysS;{gPHIsq3[YVKf*eS3zV6i7&kxBFX6xP3Zl+TM.4^lELdGP<]C' );
define( 'NONCE_KEY',        'HPjjOb1Fyp:umO{kdp*Q,8Jr`_M%8eo*`d-nU<:|=CfGuj_WUq)V#X/iA{w1~F[q' );
define( 'AUTH_SALT',        '=6t{LsR80?|.f!XHqH (vjx:P{A3b12v&qV/U.5|-B 3@HIj$|hMLqm?9>:/|k?<' );
define( 'SECURE_AUTH_SALT', '8)pv;(I<f{##7|_sWl}%$`tOhb:q;[FQPvJT)7xOz&kns59i=Cpo5(QKxwoKD9N:' );
define( 'LOGGED_IN_SALT',   'QOId/KLtyo%ew1ieU5Q3Ub[NVqEorkf}N]c6~tP(V`^z^DbaC,u2N>FS51`-)}P1' );
define( 'NONCE_SALT',       'RMU__1g;|_.8<c]%KmJ-:<daMOjbPc(/@esOU_#Jst$8K)5&h9)DbE7#> 6qUyK0' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
